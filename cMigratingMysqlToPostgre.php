<?php


ini_set("memory_limit","1024M");
ini_set('error_reporting', E_ALL ^ E_NOTICE);


class cMigratingMysqlToPostgre
{
  private $MyHost, $MyUser, $MyPass, $MyDb;
  private $PgHost, $PgUser, $PgPass, $PgDb;
  private $MyConn, $PgConn;
  private $aEnumerated = array();
  
  public function setMysqlConnection($MyHost, $MyUser, $MyPass, $MyDb)
  {
    $this->MyHost = $MyHost;
    $this->MyUser = $MyUser;
    $this->MyPass = $MyPass;
    $this->MyDb = $MyDb;
  }
  
  
  public function setPosgreConnection($MyHost, $MyUser, $MyPass, $MyDb = null)
  {
    $this->PgHost = $MyHost;
    $this->PgUser = $MyUser;
    $this->PgPass = $MyPass;
    $this->PgDb = $MyDb ? $MyDb : $MyUser;
  }
  
  
  public function startMigrate()
  {
    try
    {
      $this->connectMySql();
      $this->connectPgSql();
      
      $aMySqlTableList = $this->getTableListMysql();
      if (!$aMySqlTableList) throw new Exception('No tables to migrate');
      
      foreach ($aMySqlTableList as $sTable)
      {
        @pg_query($this->PgConn, 'DROP TABLE '.$sTable.' CASCADE');
        
        $aMysqlCellList = $this->getCellListMysql($sTable);
        
        $this->createTablePg($sTable, $aMysqlCellList);
        
        $aMysqlIndexList = $this->getIndexListMysql($sTable);
        
        $this->createIndexPg($sTable, $aMysqlIndexList);
        
        $this->addData($sTable, $aMysqlCellList);
      }
    }
    catch (Exception $e)
    {
      echo '*** error: '.$e->getMessage().' ***'."\nLine: ".$e->getLine();
      exit();
    }
  }
  
  
  private function addData($sTable, $aMysqlCellList)
  {
    if (!$res_num = mysql_query("SELECT count(*) as nb FROM ".$sTable, $this->MyConn)) throw new Exception(mysql_error($this->MyConn));
    $MyNumEntries = mysql_fetch_assoc($res_num);
    $iNumRows = $MyNumEntries['nb'];
    $this->log("There is ".number_format($iNumRows, 0)." entries in the Mysql table.");
    
    if ($iNumRows == 0) return true; // neni nic k importu
    
    $iVerbose = ceil($iNumRows/100) < 1000 ? 1000 : ceil($iNumRows/100);
    $iOffset = 0; $iDone = 0;
    $iLimit = 10000;
    do
    {
      if ($iOffset == 0) $this->log("Retrieve the content from the MySQL table...".$sTable);
      else $this->log("Retrieve more content from the MySQL table to limit memory usage...");

      $res = mysql_query("SELECT * FROM ".$sTable." LIMIT ".$iOffset.", ".$iLimit, $this->MyConn);

      while ($data = mysql_fetch_array($res, MYSQL_ASSOC))
      {
        $aDataSave = ''; $aCells = array();
        foreach ($aMysqlCellList as $sCell => $aCell)
        {
          if (($aCell['Null'] == 'YES' && $data[$sCell] == '') || ($this->translateMysqlTypeToPg($aCell['Type']) == 'timestamp' && $data[$sCell] == '0000-00-00 00:00:00')  ) $aDataSave[] = 'NULL';
          else if (stristr($aCell['Type'], 'int') || stristr($aCell['Type'], 'float') || stristr($aCell['Type'], 'double') || stristr($aCell['Type'], 'decimal')) $aDataSave[] = $data[$sCell]+0;
          else if (stristr($aCell['Type'], 'enum'))
          {
            preg_match('/enum\((.*)\)/', $aCell['Type'], $regs);
            $aValues = explode(',', $regs[1]);
            foreach ($aValues as $key => $val)
            {
              $aValues[$key] = str_replace("'", '', $val);
            }
            if (!in_array($data[$sCell], $aValues))
            {
              $data[$sCell] = reset($aValues);
            }
            $aDataSave[] = "'".pg_escape_string ($this->PgConn, $data[$sCell])."'";
          }
          else $aDataSave[] = "'".pg_escape_string ($this->PgConn, $data[$sCell])."'";
          $aCells[] = '"'.$sCell.'"';
        }
        
        $sql = 'INSERT INTO '.$sTable.' ('.implode(',', $aCells).') VALUES('.implode(',', $aDataSave).')';
        
        if (!@pg_query($sql)) throw new Exception ($sql."\n".  pg_last_error($this->PgConn));
        
        if ($iDone++ %$iVerbose == 0) $this->log('Done: '.$iDone.' - '.round((($iDone / $iNumRows) * 100),2).' %');
      }
      
      
      $iOffset += $iLimit;
    } while ($iOffset < $iNumRows);
    
  }
  
  
  private function createIndexPg($sTable, $aMysqlIndexList)
  {
    foreach ($aMysqlIndexList as $sKey => $aIndex )
    {
      $aColNames = array(); $bIsUnique = false; $bIsFulltext = false;
      foreach ($aIndex as $val)
      {
        $aColNames[] = '"'.$val['Column_name'].'"';
        if ($val['Non_unique']+0 != 1) $bIsUnique = true;
        if ($val['Index_type'] == 'FULLTEXT') $bIsFulltext = true;
      }
      
      if ($sKey == 'PRIMARY')
      {
        $sql = 'ALTER TABLE '.$sTable.' ADD PRIMARY KEY ('.implode(',', $aColNames).')';
        if (!pg_query($this->PgConn, $sql)) throw new Exception (pg_last_error());
        $this->log($sql);
      }
      else if ($bIsFulltext)
      {
        $aTsVectors = array();
        foreach ($aColNames as $col)
        {
          $aTsVectors[] = "to_tsvector('english', ".$col.")";
        }
        $sql = "CREATE INDEX ON $sTable USING gin (".implode(',', $aTsVectors)." )";
        $this->log($sql);
        if (!@pg_query($this->PgConn, $sql)) throw new Exception (' '.pg_last_error());
      }
      else if ($bIsUnique)
      {
        $sql = 'ALTER TABLE '.$sTable.' ADD UNIQUE ('.implode(',', $aColNames).')';
        if (!pg_query($this->PgConn, $sql)) throw new Exception (' '.pg_last_error());
        $this->log($sql);
      }
      else
      {
        $sql = 'CREATE INDEX ON '.$sTable.' ('.implode(',', $aColNames).')';
        if (!pg_query($this->PgConn, $sql)) throw new Exception (' '.pg_last_error());
        $this->log($sql);
      }
    }
  }
  
  
  /**
   * all timestamp values translated to NULL - I do not know, how to save value 0000-00-00 00:00:00 to DB postgre
   * @throws Exception
   */
  private function createTablePg($sTable, $aCellList)
  {
    $sPgSql = 'CREATE TABLE '.$sTable.' ('."\n";
    foreach ($aCellList as $sCellname => $cell)
    {
      $sPgSql .= '"'.$sCellname.'" ';
      
      if ($cell['Extra'] == 'auto_increment') $sPgSql .= 'SERIAL ';
      else $sPgSql .= $this->translateMysqlTypeToPg($cell['Type']).' ';
      
      $sPgSql .= ($cell['Null'] == 'YES' || $this->translateMysqlTypeToPg($cell['Type']) == 'timestamp' ? 'NULL ' : 'NOT NULL ');
      
      if ($cell['Default'] != NULL && $cell['Default'] != '0000-00-00 00:00:00')
      {
        $sPgSql .= 'DEFAULT ';
        if (stristr($this->translateMysqlTypeToPg($cell['Type']), 'int')) $sPgSql .= $cell['Default']+0;
        else if (stristr($cell['Default'], 'current_timestamp')) $sPgSql .= "NOW()";
        else $sPgSql .= "'".$cell['Default']."'";
      }
      
      $sPgSql .= ",\n";
      
    }
    $sPgSql = mb_substr($sPgSql, 0, -2, 'utf-8');
    $sPgSql .= ")";
    
    file_put_contents(__DIR__.'/txt.txt', $sPgSql);
    $this->log('create table: '.$sTable);
    if (!@pg_query($this->PgConn, $sPgSql)) throw new Exception ($sPgSql."\n".pg_last_error());
  }
  
  
  private function translateMysqlTypeToPg($sType)
  {
    if (stristr($sType, 'bigint')) return 'bigint';
    if (stristr($sType, 'smallint')) return 'smallint';
    if (stristr($sType, 'int')) return 'int';
    if (stristr($sType, 'double')) return 'float';
    if (stristr($sType, 'text')) return 'TEXT';
    if (stristr($sType, 'blob')) return 'BYTEA';
    if (stristr($sType, 'datetime')) return 'timestamp';
    if (stristr($sType, 'enum')) return $this->createEnumeratedType($sType);
    return $sType;
  }
  
  
  private function createEnumeratedType($sType)
  {
    $aValues = array();
    if (preg_match("/enum\((.*)\)/i", $sType, $regs))
    {
      $aValues = explode(',', $regs[1]);
    }
    
    $sEnum = 'enum_'.count($this->aEnumerated);
    $sValue = '';
    foreach ($aValues as $val)
    {
      $sValue .= $val.", ";
    }
    if ($sValue) $sValue = mb_substr($sValue, 0, -2, 'utf-8');

    if (isset($this->aEnumerated[$sValue])) return $this->aEnumerated[$sValue];

    @pg_query($this->PgConn, "drop type ".$sEnum." cascade");

    $sql = "CREATE TYPE ".$sEnum." AS ENUM (".$sValue.")";
    if (!@pg_query($this->PgConn, $sql))
    {
      throw new Exception('error when creating EnumeratedType:'.pg_errormessage($this->PgConn));
    }

    $this->aEnumerated[$sValue] = $sEnum;

    return $sEnum;
  }

  
  
  private function getCellListMysql($sTable)
  {
    $aList = array();
    $sql = 'SHOW COLUMNS FROM `'.$sTable.'`';
    $res = mysql_query($sql, $this->MyConn);
    while ($data = mysql_fetch_array($res, MYSQL_ASSOC))
    {
      $aList[$data['Field']] = $data;
    }
    return $aList;
  }
  
  
  private function getIndexListMysql($sTable)
  {
    $aIndexList = array();
    $sql = 'SHOW INDEX FROM '.$sTable;
    $res = mysql_query($sql, $this->MyConn);
    while ($data = mysql_fetch_array($res, MYSQL_ASSOC))
    {
      $aIndexList[$data['Key_name']][$data['Seq_in_index']] = $data;
    }
    return $aIndexList;
  }
  
  
  private function getTableListMysql()
  {
    $res_tables = mysql_query("SHOW TABLES FROM `".$this->MyDb."`", $this->MyConn);
        
    if (!$res_tables) 
    {
      throw new Exception(" Error: Impossible to list MySQL tables for the database \"".$this->MyDb."\"\n");
    }
    while ($row_tables = mysql_fetch_row($res_tables)) 
    {
      $aList[] = $row_tables[0];
    }
    return $aList;
  }
  
  
  private function connectMySql()
  {
    $this->MyConn = mysql_connect($this->MyHost, $this->MyUser, $this->MyPass);
    if (!$this->MyConn) throw new Exception("MySql connection impossible : ".mysql_error());
    if (!mysql_select_db($this->MyDb, $this->MyConn)) 
    {
      throw new Exception("Error: Impossible to select MySQL database \"".$this->MyDB."\"\n".  mysql_error());
    }
    mysql_query('SET NAMES utf8', $this->MyConn);
    mysql_query('SET CHARACTER SET "utf8"', $this->MyConn);
  }
  
  
  private function connectPgSql()
  {
    $this->PgConn = pg_connect("host=".$this->PgHost." port=5432 user=".$this->PgUser." password=".$this->PgPass." dbname=".$this->PgDb);
    if (!$this->PgConn) throw new Exception("PgSql connection impossible : ". pg_last_error());
  }
  
  
  private function log($s)
  {
    echo '* '.$s."\n";
  }
}